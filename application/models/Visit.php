<?php
class Visit extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getTotal2020()
    {
        $sql = "select
        YEAR(fecha_con) AS ano,
        MONTH(fecha_con) AS mes,
        COUNT(*) AS total_visitas
    FROM
        contador
    WHERE
        YEAR(fecha_con) = '2020'
    GROUP BY
        YEAR(fecha_con),
        MONTH(fecha_con)
    ORDER BY
        ano, mes;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalVisits2020()
    {
        $sql = "select
        SUM(total_visitas) AS total_visitas_anio
    FROM (
        SELECT
            YEAR(fecha_con) AS ano,
            MONTH(fecha_con) AS mes,
            COUNT(*) AS total_visitas
        FROM
            contador
        WHERE
            YEAR(fecha_con) = '2020'
        GROUP BY
            YEAR(fecha_con),
            MONTH(fecha_con)
    ) AS subconsulta
    GROUP BY
        ano
    ORDER BY
        ano;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->total_visitas_anio;
        } else {
            return 0;
        }
    }

    function getTotalVisits2021()
    {
        $sql = "select
        SUM(total_visitas) AS total_visitas_anio
    FROM (
        SELECT
            YEAR(fecha_con) AS ano,
            MONTH(fecha_con) AS mes,
            COUNT(*) AS total_visitas
        FROM
            contador
        WHERE
            YEAR(fecha_con) = '2021'
        GROUP BY
            YEAR(fecha_con),
            MONTH(fecha_con)
    ) AS subconsulta
    GROUP BY
        ano
    ORDER BY
        ano;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->total_visitas_anio;
        } else {
            return 0;
        }
    }
    function getTotal2021()
    {
        $sql = "select
        YEAR(fecha_con) AS ano,
        MONTH(fecha_con) AS mes,
        COUNT(*) AS total_visitas
    FROM
        contador
    WHERE
        YEAR(fecha_con) = '2021'
    GROUP BY
        YEAR(fecha_con),
        MONTH(fecha_con)
    ORDER BY
        ano, mes;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalVisits2022()
    {
        $sql = "select
        SUM(total_visitas) AS total_visitas_anio
    FROM (
        SELECT
            YEAR(fecha_con) AS ano,
            MONTH(fecha_con) AS mes,
            COUNT(*) AS total_visitas
        FROM
            contador
        WHERE
            YEAR(fecha_con) = '2022'
        GROUP BY
            YEAR(fecha_con),
            MONTH(fecha_con)
    ) AS subconsulta
    GROUP BY
        ano
    ORDER BY
        ano;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->total_visitas_anio;
        } else {
            return 0;
        }
    }

    function getTotal2022()
    {
        $sql = "select
        YEAR(fecha_con) AS ano,
        MONTH(fecha_con) AS mes,
        COUNT(*) AS total_visitas
    FROM
        contador
    WHERE
        YEAR(fecha_con) = '2022'
    GROUP BY
        YEAR(fecha_con),
        MONTH(fecha_con)
    ORDER BY
        ano, mes;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalVisits2023()
    {
        $sql = "select
        SUM(total_visitas) AS total_visitas_anio
    FROM (
        SELECT
            YEAR(fecha_con) AS ano,
            MONTH(fecha_con) AS mes,
            COUNT(*) AS total_visitas
        FROM
            contador
        WHERE
            YEAR(fecha_con) = '2023'
        GROUP BY
            YEAR(fecha_con),
            MONTH(fecha_con)
    ) AS subconsulta
    GROUP BY
        ano
    ORDER BY
        ano;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row()->total_visitas_anio;
        } else {
            return 0;
        }
    }

    function getTotal2023()
    {
        $sql = "select
        YEAR(fecha_con) AS ano,
        MONTH(fecha_con) AS mes,
        COUNT(*) AS total_visitas
    FROM
        contador
    WHERE
        YEAR(fecha_con) = '2023'
    GROUP BY
        YEAR(fecha_con),
        MONTH(fecha_con)
    ORDER BY
        ano, mes;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getNotifications($order, $limit)
    {
        $sql = "select
        s.codigo_usu,
        COUNT(n.codigo_not) AS cantidad_notificaciones
      FROM
        solicitud_permiso s
      LEFT JOIN
        notificacion n ON s.codigo_sol = n.codigo_sol
      GROUP BY
        s.codigo_usu
      ORDER BY
        cantidad_notificaciones $order
      LIMIT $limit;";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }
}
