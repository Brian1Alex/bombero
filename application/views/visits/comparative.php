<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card-body">
                <h5 class="card-title">
                    <?php echo $totalVisists2020; ?>
                </h5>
                <p class="card-text">Total Visits in 2020</p>
            </div>
            <h1>VISITS IN 2020</h1>
            <canvas id="line2020" width="50%" height="50%"></canvas>

        </div>
        <div class="col-md-3">
        <div class="card-body">
                <h5 class="card-title">
                    <?php echo $totalVisists2021; ?>
                </h5>
                <p class="card-text">Total Visits in 2021</p>
            </div>
            <h1>VISITS IN 2021</h1>
            <canvas id="line2021" width="50%" height="50%"></canvas>

        </div>
        <div class="col-md-3">
        <div class="card-body">
                <h5 class="card-title">
                    <?php echo $totalVisists2022; ?>
                </h5>
                <p class="card-text">Total Visits in 2022</p>
            </div>
            <h1>VISITS IN 2022</h1>
            <canvas id="line2022" width="50%" height="50%"></canvas>

        </div>
        <div class="col-md-3">
        <div class="card-body">
                <h5 class="card-title">
                    <?php echo $totalVisists2023; ?>
                </h5>
                <p class="card-text">Total Visits in 2023</p>
            </div>
            <h1>VISITS IN 2023</h1>
            <canvas id="line2023" width="50%" height="50%"></canvas>

        </div>
    </div>
</div>

<script type="text/javascript">
    var datos = {
        labels: [
            <?php if ($productList2020) : ?>
                <?php foreach ($productList2020 as $product) : ?> '<?php echo $product->mes; ?>',
                <?php endforeach; ?>
            <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productList2020) : ?>
                    <?php foreach ($productList2020 as $product) : ?> '<?php echo $product->total_visitas; ?>',
                    <?php endforeach; ?>
                <?php endif; ?>

            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('line2020').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'line',
        data: datos,
        options: opciones
    });
</script>
<script type="text/javascript">
    var datos = {
        labels: [
            <?php if ($productList2021) : ?>
                <?php foreach ($productList2021 as $product) : ?> '<?php echo $product->mes; ?>',
                <?php endforeach; ?>
            <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productList2021) : ?>
                    <?php foreach ($productList2021 as $product) : ?> '<?php echo $product->total_visitas; ?>',
                    <?php endforeach; ?>
                <?php endif; ?>

            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('line2021').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'line',
        data: datos,
        options: opciones
    });
</script>
<script type="text/javascript">
    var datos = {
        labels: [
            <?php if ($productList2022) : ?>
                <?php foreach ($productList2022 as $product) : ?> '<?php echo $product->mes; ?>',
                <?php endforeach; ?>
            <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productList2022) : ?>
                    <?php foreach ($productList2022 as $product) : ?> '<?php echo $product->total_visitas; ?>',
                    <?php endforeach; ?>
                <?php endif; ?>

            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('line2022').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'line',
        data: datos,
        options: opciones
    });
</script>
<script type="text/javascript">
    var datos = {
        labels: [
            <?php if ($productList2023) : ?>
                <?php foreach ($productList2023 as $product) : ?> '<?php echo $product->mes; ?>',
                <?php endforeach; ?>
            <?php endif; ?>
        ],
        datasets: [{
            label: 'Datos de ejemplo',
            data: [
                <?php if ($productList2023) : ?>
                    <?php foreach ($productList2023 as $product) : ?> '<?php echo $product->total_visitas; ?>',
                    <?php endforeach; ?>
                <?php endif; ?>

            ], // Valores de las barras
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)', // Color de la primera barra
                'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
                'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    };

    // Opciones de configuraci�n
    var opciones = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('line2023').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
        type: 'line',
        data: datos,
        options: opciones
    });
</script>