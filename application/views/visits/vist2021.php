<div class="container">
	<div class="row">
		<div class="col-md-7">
			<div class="card-body">
				<h5 class="card-title">
					<?php echo $totalVisists2021; ?>
				</h5>
				<p class="card-text">Total Visits in 2021</p>
			</div>

			<h1>VISITS IN 2021</h1>
			<canvas id="line2020" width="50%" height="50%"></canvas>
		</div>
		<div class="col-md-5">
			<div class="col-md-12">
				<h3>TOTAL OF VISITS</h3>
				<?php if ($productList) : ?>
					<table class="table" id="tbl_product">
						<thead>
							<tr>

								<th>YEAR</th>
								<th>MONTH</th>
								<th>TOTAL</th>

							</tr>
						</thead>
						<tbody>
							<?php foreach ($productList as $p) : ?>
								<tr>

									<td><?php echo $p->ano ?></td>
									<td><?php echo $p->mes ?></td>
									<td><?php echo $p->total_visitas ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else : ?>
					<h3>No Visits in Database</h3>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#tbl_product").DataTable();
</script>

<script type="text/javascript">
	var datos = {
		labels: [
			<?php if ($productList) : ?>
				<?php foreach ($productList as $product) : ?> '<?php echo $product->mes; ?>',
				<?php endforeach; ?>
			<?php endif; ?>
		],
		datasets: [{
			label: 'Datos de ejemplo',
			data: [
				<?php if ($productList) : ?>
					<?php foreach ($productList as $product) : ?> '<?php echo $product->total_visitas; ?>',
					<?php endforeach; ?>
				<?php endif; ?>

			], // Valores de las barras
			backgroundColor: [
				'rgba(255, 99, 132, 0.6)', // Color de la primera barra
				'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
				'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
			],
			borderColor: [
				'rgba(255, 99, 132, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)'
			],
			borderWidth: 1
		}]
	};

	// Opciones de configuraci�n
	var opciones = {
		scales: {
			y: {
				beginAtZero: true
			}
		}
	};

	// Obtener el contexto del lienzo
	var contexto = document.getElementById('line2020').getContext('2d');

	// Crear el gr�fico de barras
	var graficoDeBarras = new Chart(contexto, {
		type: 'line',
		data: datos,
		options: opciones
	});
</script>