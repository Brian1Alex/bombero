<?php
    class Visits extends CI_Controller
    {
        public function __construct(){
            parent::__construct(); 
            $this->load->model("Visit");
        }

        public function vist2020()
	{
		$data["totalVisists2020"] = $this->Visit->getTotalVisits2020();
		$data["productList"] = $this->Visit->getTotal2020();
		$this->load->view('header');
		$this->load->view('visits/vist2020', $data);
		$this->load->view('footer');
	}

    public function vist2021()
	{
		$data["totalVisists2021"] = $this->Visit->getTotalVisits2021();
		$data["productList"] = $this->Visit->getTotal2021();
		$this->load->view('header');
		$this->load->view('visits/vist2021', $data);
		$this->load->view('footer');
	}

    public function vist2022()
	{
		$data["totalVisists2022"] = $this->Visit->getTotalVisits2022();
		$data["productList"] = $this->Visit->getTotal2022();
		$this->load->view('header');
		$this->load->view('visits/vist2022', $data);
		$this->load->view('footer');
	}

    public function vist2023()
	{
		$data["totalVisists2023"] = $this->Visit->getTotalVisits2023();
		$data["productList"] = $this->Visit->getTotal2023();
		$this->load->view('header');
		$this->load->view('visits/vist2023', $data);
		$this->load->view('footer');
	}

    public function comparative()
	{
        $data["totalVisists2020"] = $this->Visit->getTotalVisits2020();
		$data["productList2020"] = $this->Visit->getTotal2020();
        $data["totalVisists2021"] = $this->Visit->getTotalVisits2021();
		$data["productList2021"] = $this->Visit->getTotal2021();
        $data["totalVisists2022"] = $this->Visit->getTotalVisits2022();
		$data["productList2022"] = $this->Visit->getTotal2022();
		$data["totalVisists2023"] = $this->Visit->getTotalVisits2023();
		$data["productList2023"] = $this->Visit->getTotal2023();
		
		$this->load->view('header');
		$this->load->view('visits/comparative', $data);
		$this->load->view('footer');
	}


    }
?>