<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Visit');
	}
	public function index()
	{
		$data["totalVisists2020"] = $this->Visit->getTotalVisits2020();
		$data["productList2020"] = $this->Visit->getTotal2020();
        $data["totalVisists2021"] = $this->Visit->getTotalVisits2021();
		$data["productList2021"] = $this->Visit->getTotal2021();
        $data["totalVisists2022"] = $this->Visit->getTotalVisits2022();
		$data["productList2022"] = $this->Visit->getTotal2022();
		$data["totalVisists2023"] = $this->Visit->getTotalVisits2023();
		$data["productList2023"] = $this->Visit->getTotal2023();
		$data["topNotifications"] = $this->Visit->getNotifications("desc", 5);
		$this->load->view('header');
		$this->load->view('welcome_message', $data);
		$this->load->view('footer');
	}

	
}
